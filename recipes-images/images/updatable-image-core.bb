#
# Copyright Prd-Flash Contributors
#
# SPDX-License-Identifier: MIT
#

SUMMARY = "Updatable Image - Core"
LICENSE = "MIT"
PR="r0"

LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

inherit updatable-image

# Add extra image features
EXTRA_IMAGE_FEATURES += " \
	${@bb.utils.contains('WANTED_ROOT_DEV', 'nfs', '', 'ssh-server-dropbear', d) } \
	updatable-base \
"
