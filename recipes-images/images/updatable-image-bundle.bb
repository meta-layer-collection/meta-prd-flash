#
# Copyright Prd-Flash Contributors
#
# SPDX-License-Identifier: MIT
#

SUMMARY = "Updatable Image - Wic Bundle"
LICENSE = "MIT"
PR="r0"

LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

inherit updatable-bundle-wic-image

require ${UPDATABLE_ROOTFS_IMAGE}.bb
