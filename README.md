OpenEmbedded/Yocto Project for flashing devices in multiple situations
======================================================================

This layer provides support for updatable multifs images for use with
Yocto Project build systems.

This layer requires few additions to poky maintained in

URI: https://git.yoctoproject.org/poky
branch: master
revision: HEAD

Contributing:
-------------

You can submit Merge Requests via GitLab using
https://gitlab.com/meta-layer-collection/meta-prd-flash/-/merge_requests

Please refer to:
https://wiki.yoctoproject.org/wiki/Contribution_Guidelines#General_Information

for some useful guidelines to be followed when submitting patches.

Usage instructions
------------------

Specify update configuration via your _distro/my-distro.conf_ like:

    UPDATABLE_IMAGE_CLASSES = "my-distro-image-extra"
    
    UPDATABLE_KEY_FILE ?= "${MYDISTRO_EXAMPLES_CERT_DIR}/dev/private/development-1.key.pem"
    UPDATABLE_CERT_FILE ?= "${MYDISTRO_EXAMPLES_CERT_DIR}/dev/development-1.cert.pem"

See _classes/updatable-images-base.bbclass_ for detailed list of settings to tune
the image creation, bundle composition and applying the images and processings to
the target device.

Add information regarding the supported devices to your machine configuration like:

    AVAIL_ROOT_DEVS = "usb sd emmc"
    BOOTABLE_ROOT_DEVS = "${INTERNAL_ROOT_DEV} sd"
    INTERNAL_ROOT_DEV = "emmc"
    WANTED_ROOT_DEV ??= "${INTERNAL_ROOT_DEV}"
    
    ROOT_DEV_NAME-emmc = "mmcblk${KERNEL_MMC_DEV-emmc}"
    ROOT_DEV_SEP-emmc = "p"
    ROOT_DEV_TYPE-emmc = "ssd"
    ROOT_DEV_NAME-sd = "mmcblk${KERNEL_MMC_DEV-sd}"
    ROOT_DEV_SEP-sd = "p"
    ROOT_DEV_TYPE-sd = "ssd"
    ROOT_DEV_NAME-usb = "sda"
    ROOT_DEV_SEP-usb = ""
    ROOT_DEV_TYPE-usb = "ssd"
    
    ROOT_DEV_NAME = "${ROOT_DEV_NAME-${WANTED_ROOT_DEV}}"
    ROOT_DEV_SEP = "${ROOT_DEV_SEP-${WANTED_ROOT_DEV}}"
    ROOT_DEV_TYPE = "${ROOT_DEV_TYPE-${WANTED_ROOT_DEV}}"
    
    include conf/machine/include/my-machine-container.inc

Define details regarding used target layout might be specified in that include and
(partially) re-used:

    UPDATABLE_BUNDLE_HOOKS = "appboot prepare_overlay uboot_setenv ..."
    
    UPDATABLE_BUNDLE_HOOK_appboot[type] = "inline"
    UPDATABLE_BUNDLE_HOOK_appboot[trigger] = "postdeploy postupdate"
    UPDATABLE_BUNDLE_HOOK_appboot[code] = "if test "${UPDATE_APP}" = Y; then mv ${MNT_BOOT}/${KERNEL_IMAGETYPE}-${MACHINE} ${MNT_BOOT}/${KERNEL_IMAGETYPE}; fi"
    
    UPDATABLE_BUNDLE_HOOK_prepare_overlay[type] = "inline"
    UPDATABLE_BUNDLE_HOOK_prepare_overlay[trigger] = "postdeploy"
    UPDATABLE_BUNDLE_HOOK_prepare_overlay[code] = "mkdir -p ${MNT_DATA}/{${OVERLAY_SHADOWS}}"
    ...
    UPDATABLE_BUNDLE_SLOTS = "SPL TPL BOOTSCRIPT KERNEL DTB"
    
    UPDATABLE_SLOT_SPL = "${PREFERRED_PROVIDER_virtual/bootloader}"
    UPDATABLE_SLOT_SPL[type] = "boot"
    UPDATABLE_SLOT_SPL[file] = "${SPL_BINARY}"
    UPDATABLE_SLOT_SPL[dest] = "raw"
    UPDATABLE_SLOT_SPL[offset] = "256;512"
    UPDATABLE_SLOT_SPL[condition] = "require_update_uboot"
    UPDATABLE_SLOT_SPL[hook] = "uboot_setenv"
    
    ...
    UPDATABLE_SLOT_BOOTSCRIPT = "bootscript-${MACHINE}-${WANTED_ROOT_DEV}"
    UPDATABLE_SLOT_BOOTSCRIPT[type] = "boot"
    UPDATABLE_SLOT_BOOTSCRIPT[file] = "bootscript"
    UPDATABLE_SLOT_BOOTSCRIPT[dest] = "/boot"
    UPDATABLE_SLOT_BOOTSCRIPT[together] = "KERNEL"
    UPDATABLE_SLOT_KERNEL = "${PREFERRED_PROVIDER_virtual/kernel}"
    UPDATABLE_SLOT_KERNEL[type] = "boot"
    UPDATABLE_SLOT_KERNEL[file] = "${KERNEL_IMAGETYPE}"
    UPDATABLE_SLOT_KERNEL[rename] = "${KERNEL_IMAGETYPE}-${MACHINE}"
    UPDATABLE_SLOT_KERNEL[dest] = "/boot"
    UPDATABLE_SLOT_DTB = "${PREFERRED_PROVIDER_virtual/kernel}"
    UPDATABLE_SLOT_DTB[type] = "boot"
    UPDATABLE_SLOT_DTB[file] = "${KERNEL_DEVICETREE}"
    UPDATABLE_SLOT_DTB[dest] = "/boot"
    UPDATABLE_SLOT_DTB[together] = "KERNEL"
    
    # coming with meta-prd-flash
    include conf/machine/include/updatable-container-default-spec.inc

Either add `WANTED_ROOT_DEV` to `BB_ENV_EXTRAWHITE` or manage it in _conf/local.conf_.

