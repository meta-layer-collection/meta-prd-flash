ifndef::imagesdir[:imagesdir: ../images]

[[section-system-scope-and-context]]
== System Scope and Context



=== Business Context

[options="header",cols="1,5"]
|===
| Actor | Description
| Release Manager | As a release manager, I want to ensure that the matching components are distributed together indivisibly.
| Installer | As an installer I want to easily, reliably and quickly apply available software to my entire system
|===

[plantuml,{plantUMLDir}/business-context-overview-generate,png]
----
@startuml
!include <tupadr3/common>

!include <c4/C4_Container.puml>
!include <c4/C4_Context.puml>

SHOW_PERSON_OUTLINE()

Person(rm, "Release Manager", "Responsible to release software or devices and communicate te release to stakeholders")

Container(bundle, "Matching Components", "", "Contains components that are integrally linked for a specific goal")

System_Boundary(iot_cloud, "IoT Cloud") {
    System_Ext(repository, "Artefact Repository", "Contains different Containers and provided controlled and structured access")
}

Rel_R(rm, bundle, "takes")
Rel_R(bundle, repository, "uploads")

@enduml
----

[plantuml,{plantUMLDir}/business-context-overview-use,png]
----
@startuml
!include <tupadr3/common>

!include <c4/C4_Container.puml>
!include <c4/C4_Context.puml>
!include <c4/C4_Deployment.puml>

SHOW_PERSON_OUTLINE()

Person(installer, "Installer", "One who wants to install software on a device")

Deployment_Node(machine, "Physical Device", "The one who gets some software installed")

Container(bundle, "Matching Components", "", "Contains components that are integrally linked for a specific goal")

System_Boundary(iot_cloud, "IoT Cloud") {
    System_Ext(repository, "Artefact Repository", "Contains different Containers and provided controlled and structured access")
}

Rel_D(installer, machine, "wants update")
Rel_D(machine, repository, "request availability")
Rel_L(repository, bundle, "chooses")
Rel_U(bundle, machine, "downloads")

@enduml
----

=== Technical Context

[plantuml,{plantUMLDir}/technical-context-overview-generate,png]
----
@startuml
!include <tupadr3/common>

!include <c4/C4_Container.puml>
!include <c4/C4_Component.puml>
!include <c4/C4_Context.puml>
!include <c4/C4_Deployment.puml>

SHOW_PERSON_OUTLINE()

Person_Ext(sw_developer, "Software Developer", "Responsible to add new features to the software")
Person(yocto_developer, "Yocto Developer", "Responsible to integrate new software into the operating system")

Component(software, "Particular software", "Programming language", "Software to fulfill a particular business")

ComponentDb(meta_layer, "Yocto meta layer", "Recipes", "Manages several software components and their dependencies")

Node(build_node, "Build node", "Bitbake", "Computes software source into executable binaries and package everything which belongs together into appropriate containers")

Container(bundle, "Matching Components", "", "Contains components that are integrally linked for a specific goal")

System_Boundary(iot_cloud, "IoT Cloud") {
    System_Ext(repository, "Artefact Repository", "Contains different Containers and provided controlled and structured access")
}

Rel_D(sw_developer, software, "Develops")
Rel_U(software, yocto_developer, "takes")
Rel_D(yocto_developer, meta_layer, "integrates")
Rel_D(meta_layer, build_node, "compiles")
Rel_R(build_node, bundle, "deploys")
Rel_D(bundle, repository, "uploads")

@enduml
----
