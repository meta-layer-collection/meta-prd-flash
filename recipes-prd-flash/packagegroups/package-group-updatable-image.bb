#
# Copyright Prd-Flash Contributors
#
# SPDX-License-Identifier: MIT
#

SUMMARY = "Image update tools"
DESCRIPTION = "Package group for updatable image"
HOMEPAGE = "https://github.com/rehsack/"

inherit packagegroup

PROVIDES = "${PACKAGES}"
PACKAGES = "${PN}-base"

RDEPENDS_${PN}-base = "\
	prd-flash-${WANTED_ROOT_DEV} \
"
